//
//  PPTilePageViewController.m
//  Plasma
//
//  Created by Mike Mayer on 2/23/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import "PPTilePageViewController.h"

@interface PPTilePageViewController ()

@end

@implementation PPTilePageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		self.delegate = self;
		self.dataSource = self;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  PPDetailViewController.h
//  Plasma
//
//  Created by Mike Mayer on 2/23/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPEvent.h"

@interface PPDetailViewController : UIViewController
{
	NSURL *detailUrl;
}

- (IBAction)browseToEventUrl:(id)sender;
- (IBAction)backToSummary:(id)sender;

-(void) populateEvent:(PPEvent*)event;

@property NSURL *detailUrl;

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

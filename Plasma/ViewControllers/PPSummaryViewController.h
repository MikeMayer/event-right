//
//  PPSummaryViewController.h
//  Plasma
//
//  Created by Mike Mayer on 2/23/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <NVUIGradientButton/NVUIGradientButton.h>

#import "PPEvents.h"
#import "PPEvent.h"
#import "../Classes/PPLocalEventbriteReader.h"

@interface PPSummaryViewController : UIViewController
{
	PPEvents *eventCollection;
	PPEvent *currentEvent;
	NSNumber *eventIndex;
}

@property PPEvent *currentEvent;
@property PPEvents *eventCollection;
@property NSNumber *eventIndex;
@property (weak, nonatomic) IBOutlet UIView *eventTitleContainer;

-(void) populateEventData:(NSNumber*)index;

-(void)eventDidChange: (PPEvent*)event;

- (IBAction)timeFilter:(id)sender;
- (IBAction)locationFilter:(id)sender;
- (IBAction)priceFilter:(id)sender;
- (IBAction)categoryFilter:(id)sender;

- (IBAction)notEverTapped:(id)sender;
- (IBAction)notNowTapped:(id)sender;
- (IBAction)detailsRequested:(id)sender;

- (IBAction)settingsTapped:(id)sender;

@end

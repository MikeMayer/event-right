//
//  PPSummaryViewController.m
//  Plasma
//
//  Created by Mike Mayer on 2/23/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "PPSummaryViewController.h"
#import "PPDetailViewController.h"
#import "PPEventTileController.h"

@interface PPSummaryViewController ()

@end

@implementation PPSummaryViewController

@synthesize eventCollection, eventIndex, currentEvent;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		eventIndex = 0;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	eventCollection = [PPLocalEventbriteReader readEvents:[NSNumber numberWithInt:0]];
	
	[self populateEventData: [NSNumber numberWithInt:0]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) populateEventData:(NSNumber*)index {
	@try {
		if ([index intValue] >= [eventCollection count]) {
			index = [NSNumber numberWithInt:0];		
			eventCollection = [PPLocalEventbriteReader readEvents:[NSNumber numberWithInt:[[eventCollection page] intValue] + 1]];
		}

		if (!eventCollection) {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No More Events" message:@"We couldn't find any more data to show you :/" delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
			
			[alert show];
			return;
		}
		
		currentEvent = [eventCollection.events objectAtIndex:[index intValue]];
		
		[self eventDidChange:currentEvent];
	}
	@catch (NSException  *exception) {
		NSLog(@"Couldn't populate Event, %@", exception);
	}
}

-(void)eventDidChange: (PPEvent*)event {
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];

	PPEventTileController *eventTile = [storyboard instantiateViewControllerWithIdentifier:@"eventTile"];
		
	eventTile.view.frame = CGRectMake(0,0,300,300);
	eventTile.event = event;
	
	[eventTile populateEvent];

	eventTile.view.alpha = 0.0;
	[_eventTitleContainer addSubview:eventTile.view];
	
	
	[UIView animateWithDuration:0.25
						  delay:0.0
						options: UIViewAnimationOptionCurveEaseIn
					 animations:^{
						 eventTile.view.alpha = 0.0;
					 }
					 completion:^(BOOL finished){
						 [UIView animateWithDuration:0.25
											   delay:0.0
											 options:UIViewAnimationOptionCurveEaseOut
										  animations:^{
											  eventTile.view.alpha = 1.0;
										  }
										  completion:nil];
					 }];
}


- (IBAction)timeFilter:(id)sender {
	UIActionSheet *sheet = [[UIActionSheet alloc]
							initWithTitle:@"Select Time"
										delegate:nil
							   cancelButtonTitle:@"Cancel"
						  destructiveButtonTitle:nil
							   otherButtonTitles:@"Today", @"Tomorrow", @"This Week", @"...", nil];
	[sheet showInView:self.view];
}

- (IBAction)locationFilter:(id)sender {
	UIActionSheet *sheet = [[UIActionSheet alloc]
							initWithTitle:@"Select Location Range"
							delegate:nil
							cancelButtonTitle:@"Cancel"
							destructiveButtonTitle:nil
							otherButtonTitles:@"1 Mile", @"5 Miles", @"15 Miles", @"...", nil];
	[sheet showInView:self.view];
}

- (IBAction)priceFilter:(id)sender {
	UIActionSheet *sheet = [[UIActionSheet alloc]
							initWithTitle:@"Select Price Range"
							delegate:nil
							cancelButtonTitle:@"Cancel"
							destructiveButtonTitle:nil
							otherButtonTitles:@"Free", @"Up to $5", @"Up to $20", @"...", nil];
	[sheet showInView:self.view];
}

- (IBAction)categoryFilter:(id)sender {
	UIActionSheet *sheet = [[UIActionSheet alloc]
							initWithTitle:@"Select A Category"
							delegate:nil
							cancelButtonTitle:@"Cancel"
							destructiveButtonTitle:nil
							otherButtonTitles:@"Classes", @"Technical", @"Fun", @"...", nil];
	[sheet showInView:self.view];
}


- (IBAction)notEverTapped:(id)sender {

	[self notNowTapped:sender];
}

- (IBAction)notNowTapped:(id)sender {
	
	eventIndex = [NSNumber numberWithInt:[eventIndex intValue] + 1];
	
	[self populateEventData:eventIndex];
	
}

- (IBAction)detailsRequested:(id)sender {
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];	
	PPDetailViewController *detailView = [storyboard instantiateViewControllerWithIdentifier:@"detailView"];
	
	detailView.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	
	
	[self presentViewController:detailView animated:YES completion:^(void) {
		[detailView populateEvent:currentEvent];
	}];
}

- (IBAction)settingsTapped:(id)sender {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Settings" message:@"This is where a new view would show to give the user to configure their preferences, connect to Facebook, etc." delegate:nil cancelButtonTitle:@"Cool" otherButtonTitles:nil];
	
	[alert show];
}
@end

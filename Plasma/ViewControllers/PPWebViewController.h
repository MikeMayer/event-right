//
//  PPWebViewController.h
//  Plasma
//
//  Created by Mike Mayer on 2/23/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PPWebViewController : UIViewController<UIWebViewDelegate>

- (IBAction)openInSafari:(id)sender;
@property (weak, nonatomic) IBOutlet UIWebView *webView;


- (void)webViewDidStartLoad:(UIWebView *)webView;
- (void)webViewDidFinishLoad:(UIWebView *)webView;

@end

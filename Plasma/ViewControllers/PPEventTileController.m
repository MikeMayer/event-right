//
//  PPEventTileController.m
//  Plasma
//
//  Created by Mike Mayer on 2/23/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "PPEventTileController.h"
#import "PPTicket.h"

@interface PPEventTileController ()

@end

@implementation PPEventTileController
@synthesize event;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		
		
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	//self.titleLabel.font = [UIFont fontWithName:@"Adelle" size:20.0f];
	
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)populateEvent
{
	if (!event) {
		return;
	}
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	
	//2013-03-16 13:00:00
	//@"yyyy-MM-dd hh:mm:ss";
	
	dateFormatter.dateFormat = @"yyyy-MM-dd hh:mm:ss";


	NSDate *eventDate = [dateFormatter dateFromString:[event.start_date description]];
	dateFormatter.dateFormat = @"MMMM d '@' h:mm a";
	
	self.titleLabel.text = event.title;
	self.dateLabel.text = [[dateFormatter stringFromDate:eventDate] uppercaseString];
	
	NSMutableArray *tagsAndCategories = [NSMutableArray arrayWithArray:event.category];
	NSMutableArray *usedStrings = [[NSMutableArray alloc] initWithCapacity:5];
	[tagsAndCategories addObjectsFromArray:event.tags];
	
	int tagLabelUsed = 0;
	_firstTagLabel.hidden = YES;
	_secondTagLabel.hidden = YES;
	_thirdTagLabel.hidden = YES;
	_fourthTagLabel.hidden = YES;
	
	for (int i = 0; i < [tagsAndCategories count]; i++) {
		NSString *tag = [[tagsAndCategories objectAtIndex:i] uppercaseString];
		if ([tag length] > 0) {
			
			switch(tagLabelUsed) {
				case 0: {
					for (int x = 0; x < [usedStrings count]; x++) {
						NSString *usedString = [usedStrings objectAtIndex:x];
						if ([usedString isEqualToString:tag]) {
							continue;
						}
					}
					_firstTagLabel.hidden = NO;
					_firstTagLabel.layer.cornerRadius = 4;
					_firstTagLabel.text = [NSString stringWithFormat:@" • %@ ", tag];
					tagLabelUsed++;
					continue;
				}
				case 1: {
					for (int x = 0; x < [usedStrings count]; x++) {
						NSString *usedString = [usedStrings objectAtIndex:x];
						if ([usedString isEqualToString:tag]) {
							continue;
						}
					}
					_secondTagLabel.hidden = NO;
					_secondTagLabel.layer.cornerRadius = 4;
					_secondTagLabel.text = [NSString stringWithFormat:@" • %@ ", tag];
					tagLabelUsed++;
					continue;
				}
				case 2: {
					for (int x = 0; x < [usedStrings count]; x++) {
						NSString *usedString = [usedStrings objectAtIndex:x];
						if ([usedString isEqualToString:tag]) {
							continue;
						}
					}
					_thirdTagLabel.hidden = NO;
					_thirdTagLabel.layer.cornerRadius = 4;
					_thirdTagLabel.text = [NSString stringWithFormat:@" • %@ ", tag];
					tagLabelUsed++;
					continue;
				}
				case 3: {
					for (int x = 0; x < [usedStrings count]; x++) {
						NSString *usedString = [usedStrings objectAtIndex:x];
						if ([usedString isEqualToString:tag]) {
							continue;
						}
					}
					_fourthTagLabel.hidden = NO;
					_fourthTagLabel.layer.cornerRadius = 4;
					_fourthTagLabel.text = [NSString stringWithFormat:@" • %@ ", tag];
					tagLabelUsed++;
					continue;
				}
				default:
				{
					break;
				}
			}
			
		} else {
			continue;
		}
	}
	
	if (!event.tickets || [event.tickets count] < 1) {
		_costLabel.text = @"FREE";
	} else {
		PPTicket *ticket = [event.tickets objectAtIndex:0];
		
		if ([[ticket price] doubleValue] < 1.0) {
			_costLabel.text = @"FREE";
		} else {
			_costLabel.text = [NSString stringWithFormat:@"$%.0lf",
						   [[ticket price] doubleValue]];
		}
	}
	
	
	if (!event.venue) {
		self.addressLabel.hidden = YES;
		self.mapView.hidden = YES;
		self.venueOrCityLabel.hidden = YES;
	} else {
		self.addressLabel.hidden = NO;
		self.mapView.hidden = NO;
		self.venueOrCityLabel.hidden = NO;
		
		self.addressLabel.text = [[NSString stringWithFormat:@"%@, %@, %@", event.venue.address, event.venue.city, event.venue.postal_code] uppercaseString];
		
		
		[_mapView addAnnotation:event.venue];
		[_mapView setCenterCoordinate:event.venue.coordinate animated:YES];
		
		MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance (event.venue.coordinate, 300, 300);
		[_mapView setRegion:region animated:NO];
		
		if (event.venue.name) {
			self.venueOrCityLabel.text = [event.venue.name uppercaseString];
		} else {
			if (event.venue.city) {
				self.venueOrCityLabel.text = [event.venue.city uppercaseString];
			} else {
				self.venueOrCityLabel.hidden = YES;
			}
		}
	}
	
}


@end

//
//  PPWebViewController.m
//  Plasma
//
//  Created by Mike Mayer on 2/23/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import "PPWebViewController.h"

@interface PPWebViewController ()

@end

@implementation PPWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		_webView.delegate = self;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)openInSafari:(id)sender {
	//[[UIApplication sharedApplication] openURL:[inRequest URL]];

}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
	
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	
}


@end

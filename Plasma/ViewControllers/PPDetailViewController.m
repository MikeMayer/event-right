//
//  PPDetailViewController.m
//  Plasma
//
//  Created by Mike Mayer on 2/23/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import "PPDetailViewController.h"

@interface PPDetailViewController ()

@end

@implementation PPDetailViewController
@synthesize detailUrl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)browseToEventUrl:(id)sender {
	@try {
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:(NSString*)detailUrl]];
	}
	@catch (NSException *exception) {
		NSLog(@"Couldn't open Safari, %@", exception);
	}
	
}

- (IBAction)backToSummary:(id)sender {
	[self dismissViewControllerAnimated:YES completion:nil];
}

-(void) populateEvent:(PPEvent*)event {
	
	detailUrl = event.url;
	
	[self.webView loadHTMLString:[event description] baseURL:nil];

	[self.mapView addAnnotation:event.venue];
	[self.mapView setCenterCoordinate:event.venue.coordinate animated:YES];
	
	MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance (event.venue.coordinate, 300, 300);
	[self.mapView setRegion:region animated:NO];
}


@end

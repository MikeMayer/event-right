//
//  PPVenue.m
//  Plasma
//
//  Created by Mike Mayer on 2/22/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import "PPVenue.h"

@implementation PPVenue

@synthesize identity, name, address, address_2, city, region, postal_code, country, country_code, lat_long, latitude, longitude, coordinate;

+(PPVenue*) venueFromDictionary: (NSDictionary*)dict
{
	PPVenue *venue = [[PPVenue alloc] init];

	@try {
		venue.identity = [dict objectForKey:@"id"];
		venue.name = [dict objectForKey:@"name"];
		venue.address = [dict objectForKey:@"address"];
		venue.address_2 = [dict objectForKey:@"address_2"];
		venue.city = [dict objectForKey:@"city"];
		venue.region = [dict objectForKey:@"region"];
		venue.postal_code = [dict objectForKey:@"postal_code"];
		venue.country = [dict objectForKey:@"country"];
		venue.country_code = [dict objectForKey:@"country_code"];
		venue.lat_long =[dict objectForKey:@"Lat-Long"];
		venue.latitude = [dict objectForKey:@"latitude"];
		venue.longitude	= [dict objectForKey:@"longitude"];
		
		CLLocationCoordinate2D coordinate;
		
		coordinate.latitude = [venue.latitude doubleValue];
		coordinate.longitude = [venue.longitude doubleValue];
		
		venue.coordinate = coordinate;
		
		return venue;
	}
	
	@catch (NSException *exception) {
		NSLog(@"Couldn't deserialize Venue: %@", exception);
		return nil;
	}
}

@end

//
//  PPEvents.m
//  Plasma
//
//  Created by Mike Mayer on 2/22/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import "PPEvents.h"

@implementation PPEvents

@synthesize events, first_event, last_event, total_items, filters, num_showing;

- (id)initWithPage:(NSNumber *) page
{
    self = [super init];
	
    if (self) {
		self.page = page;
    }
	
    return self;
}

+ (PPEvents *)eventListWithEvents:(NSArray *)events
							 page:(NSNumber*)page
					  first_event:(NSNumber *)first_event
					   last_event:(NSNumber*)last_event
						  filters:(NSDictionary*)filters
						  showing:(NSNumber*)showing
{
	PPEvents *answer = [[PPEvents alloc] initWithPage:page];
	
	answer.events = events;
	answer.first_event = first_event;
	answer.last_event = last_event;
	answer.filters = filters;
	answer.num_showing = showing;
	
	
	return answer;
}


-(int) count {
	if (events) {
		return events.count;
	}
	
	return 0;
}

@end

//
//  PPOrganizer.m
//  Plasma
//
//  Created by Mike Mayer on 2/22/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import "PPOrganizer.h"

@implementation PPOrganizer

@synthesize identity, name, description, long_description, url;

@end

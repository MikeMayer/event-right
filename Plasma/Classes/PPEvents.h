//
//  PPEvents.h
//  Plasma
//
//  Created by Mike Mayer on 2/22/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PPEvents : NSObject

@property NSNumber *first_event;
@property NSNumber *last_event;
@property NSNumber *total_items;
@property NSArray *events;
@property NSDictionary *filters;
@property NSNumber *num_showing;

@property NSNumber* page;

-(int) count;

-(id) initWithPage:(NSNumber *) page;


+ (PPEvents *)eventListWithEvents:(NSArray *)events
							 page:(NSNumber*)page
					  first_event:(NSNumber *)first_event
					   last_event:(NSNumber*)last_event
						  filters:(NSDictionary*)filters
						  showing:(NSNumber*)showing;

@end

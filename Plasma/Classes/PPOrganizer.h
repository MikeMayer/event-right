//
//  PPOrganizer.h
//  Plasma
//
//  Created by Mike Mayer on 2/22/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PPOrganizer : NSObject
{
	NSNumber *identity;
	NSString *name;
	NSString *description;
	NSString *long_description;
	NSURL *url;
}

@property NSNumber *identity;
@property NSString *name;
@property NSString *description;
@property NSString *long_description;
@property NSURL *url;

/*
"organizer": {
	"url": "http://documentary.eventbrite.com",
	"description": "Founded in 1982, the International Documentary Association (IDA) is a non-profit 501(c)(3) that promotes nonfiction filmmakers, and is dedicated to increasing public awareness for the documentary genre. At IDA, we believe that the power and artistry of the documentary art form are vital to cultures and societies globally, and we exist to serve the needs of those who create this art form. Our major program areas are: Advocacy, Filmmaker Services, Education, and Public Programs and Events.",
	"long_description": "Founded in 1982, IDA is a non-profit 501(c)(3) that promotes nonfiction filmmakers, and is dedicated to increasing public awareness for the documentary genre. At IDA, we believe that the power and artistry of the documentary art form are vital to cultures and societies globally, and we exist to serve the needs of those who create this art form. Our major program areas are: Advocacy, Filmmaker Services, Education, and Public Programs and Events.",
	"id": 117648817,
	"name": "International Documentary Association"
}
*/

@end

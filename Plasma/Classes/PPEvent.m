//
//  PPEvent.m
//  Plasma
//
//  Created by Mike Mayer on 2/22/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import "PPEvent.h"

@implementation PPEvent

@synthesize identity, title, description, start_date, end_date,timezone, capacity, created, modified, privacy, password, url, logo, logo_ssl, status, tickets, organizer, timezone_offset, repeats, venue;

@synthesize background_color, box_background_color, box_border_color, box_header_background_color, box_text_color, link_color, text_color, title_text_color;

@synthesize tags, category;

+(PPEvent*) eventFromDictionary:(NSDictionary *)dict
{
	PPEvent *event = [[PPEvent alloc] init];
	
	@try {
		event.identity = [dict objectForKey:@"id"];
		event.background_color = [dict objectForKey:@"background_color"];
		event.box_background_color = [dict objectForKey:@"box_background_color"];
		event.box_border_color = [dict objectForKey:@"box_border_color"];
		event.box_header_background_color = [dict objectForKey:@"box_header_background_color"];
		event.box_text_color = [dict objectForKey:@"box_text_color"];
		event.timezone = [dict objectForKey:@"timezone"];
		event.timezone_offset = [dict objectForKey:@"timezone_offset"];
		event.description = [dict objectForKey:@"description"];
		event.start_date = [dict objectForKey:@"start_date"];
		event.end_date	= [dict objectForKey:@"end_date"];
		event.created = [dict objectForKey:@"created"];
		event.category = [[dict objectForKey:@"category"] componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" ,"]];
		event.tags = [[dict objectForKey:@"tags"] componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" ,"]];
		event.logo = [dict objectForKey:@"logo"];
		event.logo_ssl = [dict objectForKey:@"logo_ssl"];
		event.title = [dict objectForKey:@"title"];
		event.capacity = [dict objectForKey:@"capacity"];
		event.privacy = [dict objectForKey:@"privacy"];
		event.modified = [dict objectForKey:@"modified"];
		event.password = [dict objectForKey:@"password"];
		event.repeats = [dict objectForKey:@"repeats"];
		event.link_color = [dict objectForKey:@"link_color"];
		event.text_color = [dict objectForKey:@"text_color"];
		event.title_text_color = [dict objectForKey:@"title_text_color"];
		event.url = [dict objectForKey:@"url"];
		
		return event;
	}
	
	@catch (NSException *exception) {
		NSLog(@"Couldn't read event: %@", exception);
		
		return nil;
	}
}


@end

//
//  PPTicket.m
//  Plasma
//
//  Created by Mike Mayer on 2/22/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import "PPTicket.h"

@implementation PPTicket


@synthesize identity, name, description, type, currency, price, max, min, start_date, end_date, quantity_available, quantity_sold, visible;

+(PPTicket*) ticketFromDictionary:(NSDictionary *)dict
{
	PPTicket *ticket = [[PPTicket alloc] init];
	
	@try {
		ticket.identity = [dict objectForKey:@"id"];
		ticket.start_date = [dict objectForKey:@"start_date"];
		ticket.end_date	= [dict objectForKey:@"end_date"];
		ticket.name = [dict objectForKey:@"name"];
		ticket.description = [dict objectForKey:@"description"];
		ticket.currency = [dict objectForKey:@"currency"];
		ticket.price = [dict objectForKey:@"price"];
		ticket.max = [dict objectForKey:@"max"];
		ticket.min = [dict objectForKey:@"min"];
		ticket.quantity_available = [dict objectForKey:@"quantity_available"];
		ticket.quantity_sold = [dict objectForKey:@"quantity_sold"];
		ticket.visible = (BOOL) [dict objectForKey:@"visible"];

		return ticket;
	}
	@catch (NSException *exception) {
		NSLog(@"Couldn't deserialize ticket: %@", exception);
		return nil;
	}
}


@end

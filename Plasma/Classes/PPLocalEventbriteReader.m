//
//  PPLocalEventbriteReader.m
//  Plasma
//
//  Created by Mike Mayer on 2/22/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import "PPLocalEventbriteReader.h"

@implementation PPLocalEventbriteReader


+ (PPEvents*) readEvents:(NSNumber*)atPage
{
	NSString *fileName;
	
	if ([atPage intValue] > 11)
		atPage = 0;
	
	if ([atPage intValue] < 0)
		atPage = 0;
	
	switch([atPage intValue]) {
		default:
		case 0: {
			fileName = @"event_brite_los_angeles_page_1";
			break;
		}
		case 1: {
			fileName = @"event_brite_los_angeles_page_2";
			break;
		}
		case 3: {
			fileName = @"event_brite_los_angeles_page_3";
			break;
		}
		case 4: {
			fileName = @"event_brite_los_angeles_page_4";
			break;
		}
		case 5: {
			fileName = @"event_brite_los_angeles_page_5";
			break;
		}
		case 6: {
			fileName = @"event_brite_los_angeles_page_6";
		}
		case 7: {
			fileName = @"event_brite_los_angeles_page_7";
			break;
		}
		case 8: {
			fileName = @"event_brite_los_angeles_page_8";
			break;
		}
		case 9: {
			fileName = @"event_brite_los_angeles_page_9";
			break;
		}
		case 10: {
			fileName = @"event_brite_los_angeles_page_10";
			break;
		}
		case 11: {
			fileName = @"event_brite_los_angeles_page_11";
			break;

		}
	}
	NSMutableArray *answer = [[NSMutableArray alloc] initWithCapacity:113];
	
	NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"json"];
	
	NSData *eventData = [NSData dataWithContentsOfFile:filePath];
	
	if (eventData) {
		NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:eventData options:kNilOptions error:nil];
		
		NSArray* events = [dict objectForKey:@"events"];
		
		PPEvents *eventList = [[PPEvents alloc] initWithPage:atPage];
		
		for (int i = 0; i < [events count] ; i++) {
			id entry = [events objectAtIndex:i];
					
			if (i == 0) {
				id summary = [entry objectForKey:@"summary"];
				
				eventList.total_items = [summary objectForKey:@"total_items"];
				eventList.first_event = [summary objectForKey:@"first_event"];
				eventList.last_event = [summary objectForKey:@"last_event"];
				eventList.filters = [summary objectForKey:@"filters"];
				eventList.num_showing = [summary objectForKey:@"num_showing"];				
				
			} else {
				id raw = [entry objectForKey:@"event"];
				id orgRaw = [raw objectForKey:@"organizer"];
				id ticketsRaw = [raw objectForKey:@"tickets"];
				id venueRaw = [raw objectForKey:@"venue"];
				
				
				PPEvent *event = [PPEvent eventFromDictionary:raw];
								
				if (event) {
				
					if (orgRaw) {
						PPOrganizer *organizer = [[PPOrganizer alloc] init];
						organizer.identity = [orgRaw objectForKey:@"id"];
						organizer.name = [orgRaw objectForKey:@"name"];
						organizer.url = [orgRaw objectForKey:@"url"];
						organizer.description = [orgRaw objectForKey:@"description"];
						organizer.long_description = [orgRaw objectForKey:@"long_description"];
						
						event.organizer = organizer;
					}
					
					if (venueRaw) {						
						event.venue = [PPVenue venueFromDictionary:venueRaw];
					}
					
					if (ticketsRaw) {
						NSMutableArray *tickets = [[NSMutableArray alloc] initWithCapacity:13];
						
						for (int t = 0; t < [ticketsRaw count]; t++) {
							PPTicket *ticket = [PPTicket ticketFromDictionary:
												[[ticketsRaw objectAtIndex:t] objectForKey:@"ticket"]];
							
							if (ticket) {
								[tickets addObject:ticket];
							}
						}
						event.tickets = tickets;
					}
					
					[answer addObject:event];
				}
			}
		}
		
		eventList.events = answer;
	
		return eventList;
	}
	
	return nil;
}


@end

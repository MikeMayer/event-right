//
//  PPLocalEventbriteReader.h
//  Plasma
//
//  Created by Mike Mayer on 2/22/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PPEvents.h"
#import "PPEvent.h"
#import "PPTicket.h"
#import "PPVenue.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface PPLocalEventbriteReader : NSObject

+(PPEvents*)readEvents:(NSNumber*)atPage;

@end

//
//  PPVenue.h
//  Plasma
//
//  Created by Mike Mayer on 2/22/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface PPVenue : NSObject<MKAnnotation>
{
	NSNumber *identity;
	NSString *name;
	NSString *address;
	NSString *address_2;
	NSString *city;
	NSString *region;
	NSNumber *postal_code;
	NSString *country;
	NSString *country_code;
	NSString *lat_long;
	NSNumber *longitude;
	NSNumber *latitude;
}

@property CLLocationCoordinate2D coordinate;
@property NSNumber *identity;
@property NSString *name;
@property NSString *address;
@property NSString *address_2;
@property NSString *city;
@property NSString *region;
@property NSNumber *postal_code;
@property NSString *country;
@property NSString *country_code;
@property NSString *lat_long;
@property NSNumber *longitude;
@property NSNumber *latitude;

+(PPVenue*) venueFromDictionary: (NSDictionary*)dict;

@end

//
//  PPTicket.h
//  Plasma
//
//  Created by Mike Mayer on 2/22/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PPTicket : NSObject
{
	NSNumber *identity;
	NSString *name;
	NSString *description;
	NSNumber *type;
	NSString *currency;
	NSNumber *price;
	NSNumber *max;
	NSNumber *min;
	NSDate *start_date;
	NSDate *end_date;
	NSNumber *quantity_available;
	NSNumber *quantity_sold;
	BOOL visible;
}
@property NSNumber *identity;
@property NSString *name;
@property NSString *description;
@property NSNumber *type;
@property NSString *currency;
@property NSNumber *price;
@property NSNumber *max;
@property NSNumber *min;
@property NSDate *start_date;
@property NSDate *end_date;
@property NSNumber *quantity_available;
@property NSNumber *quantity_sold;
@property BOOL visible;

+(PPTicket*) ticketFromDictionary:(NSDictionary*)dict;

/*
 "tickets":
 [
	{
	 "ticket": {
		 "description": "",
		 "end_date": "2013-02-28 19:00:00",
		 "min": 1,
		 "max": null,
		 "price": "0.00",
		 "visible": "true",
		 "currency": "USD",
		 "type": 0,
		 "id": 17295302,
		 "name": "YPT-LA Drinks after Complete Streets"
		}
	}
 ]
 */

@end

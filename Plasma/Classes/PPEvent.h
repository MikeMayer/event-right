//
//  PPEvent.h
//  Plasma
//
//  Created by Mike Mayer on 2/22/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PPVenue.h"
#import "PPOrganizer.h"

@interface PPEvent : NSObject
{
	NSNumber *identity;
	NSString *title;
	NSString *description;
	NSDate *start_date;
	NSDate *end_date;
	NSString *timezone;
	NSNumber *capacity;
	NSDate *created;
	NSDate *modified;
	NSString *privacy;
	NSString *password;
	NSURL *url;
	NSURL *logo;
	NSURL *logo_ssl;
	NSString *status;
	NSString *timezone_offset;
	NSString *repeats;
	
	NSArray *tags;
	NSArray *category;
	
	UIColor *background_color;
	UIColor *box_background_color;
	UIColor *box_border_color;
	UIColor *box_header_background_color;
	UIColor *box_text_color;
	UIColor *link_color;
	UIColor *text_color;
	
	//
	PPOrganizer *organizer;
	PPVenue	*venue;
	NSArray *tickets;

}

@property NSNumber *identity;
@property NSString *title;
@property NSString *description;
@property NSDate *start_date;
@property NSDate *end_date;
@property NSString *timezone;
@property NSNumber *capacity;
@property NSDate *created;
@property NSDate *modified;
@property NSString *privacy;
@property NSString *password;
@property NSURL *url;
@property NSURL *logo;
@property NSURL *logo_ssl;
@property NSString *status;
@property NSString *repeats;

@property NSString *timezone_offset;

	//
@property PPOrganizer *organizer;
@property PPVenue *venue;
@property NSArray *tickets;
@property NSArray *tags;
@property NSArray *category;


@property UIColor *background_color;
@property UIColor *box_background_color;
@property UIColor *box_border_color;
@property UIColor *box_header_background_color;
@property UIColor *box_text_color;
@property UIColor *link_color;
@property UIColor *text_color;
@property UIColor *title_text_color;


+(PPEvent*) eventFromDictionary :(NSDictionary*)dict;



@end

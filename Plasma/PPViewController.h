//
//  PPViewController.h
//  Plasma
//
//  Created by Mike Mayer on 2/22/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Classes/PPLocalEventbriteReader.h"

@interface PPViewController : UIViewController
- (IBAction)didClickDeserialize:(id)sender;

@end

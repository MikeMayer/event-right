//
//  PPViewController.m
//  Plasma
//
//  Created by Mike Mayer on 2/22/13.
//  Copyright (c) 2013 Mike Mayer. All rights reserved.
//

#import "PPViewController.h"
#import "ViewControllers/PPWebViewController.h"

@interface PPViewController ()

@end

@implementation PPViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didClickDeserialize:(id)sender {
	[PPLocalEventbriteReader readEvents:[NSNumber numberWithInt:1]];
	
}

@end
